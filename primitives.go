package primitives

type CoordinateType interface {
	int16 | int | int32 | float32 | int64 | float64
}
type CoordinateTypeFloat interface {
	float32 | float64
}
type CoordinateTypeInt interface {
	int16 | int | int32 | int64
}

type Vec2[CT CoordinateType] struct {
	X, Y CT
}

type Size[CT CoordinateType] struct {
	Width  CT
	Height CT
}

type Line[CT CoordinateType] struct {
	Start Vec2[CT]
	End   Vec2[CT]
}

type Ray2[CT CoordinateType] struct {
	Origin 		Vec2[CT]
	Direction Vec2[CT]
}

type Triangle[CT CoordinateType] struct {
	A CT
	B CT
	C CT
}

type BoundingRect[CT CoordinateType] struct {
	X CT
	Y CT
	Height CT
	Width CT
}

type Circle[CT CoordinateType] struct {
	Center Vec2[CT]
}

type Matrix2x2[CT CoordinateType] [2][2]CT

